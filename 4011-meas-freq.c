/* \file 4011-meas-freq.c
 *
 * \brief Toggle a LED twice per second
 *
 *
 * Author: jjmcd
 *
 * Created on September 3, 2012, 10:23 AM
 */

#include <p30Fxxxx.h>

// Configuration fuses
_FOSC (FRC_PLL16 & PRI) // 7.3728 rock * 16 = 118MHz
_FWDT (WDT_OFF) // Watchdog timer off
_FBORPOR (PWRT_16 & PBOR_OFF & MCLR_EN) // Brownout off, powerup 16ms
_FGS (GWRP_OFF & CODE_PROT_OFF) // No code protection

// Mainline
int main (void)
{
  // Make PORTD LED pins outputs
  TRISD &= 0xfff1;
  // Turn off the LEDs
  LATD |= 0x000e;
  
  // Set up timer
  // 7.3728 MHz * 16xPLL /Fosc/4 / 256 prescaler / 57600 counter
  // timer should fire twice per second.  Since the LED
  // will toggle once per interrupt, the LED should come on
  // once per second for a half second.
  TMR2 = 0;             // Clear timer 2
  PR2 = 57600;          // Timer 2 counter to 576000
  PR2 = 576;          // Timer 2 counter to 576000
  T2CON = 0x8030;       // Fosc/4, 1:256 prescale, start TMR2
  IEC0bits.T2IE = 1;    // Enable timer interrupt

  // We're just going to do nothing except wait for interrupts
  while (1)
    {

    }
}

// Timer 2 interrupt routine - toggle the red (bottom) LED
// each time the interrupt occurs.
void __attribute__((__interrupt__, auto_psv)) _T2Interrupt( void )
{
    IFS0bits.T2IF = 0;      // Clear timer interrupt flag
                            // This is always the first order of
                            // business in an interrupt routine

    LATD ^= 0x0002;         // Toggle red LED
}
